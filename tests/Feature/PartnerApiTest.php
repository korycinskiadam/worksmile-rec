<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class PartnerApiTest extends TestCase
{

    use RefreshDatabase, WithFaker;


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_server_response()
    {
        $response = $this->get('/api/partner');

        $response->assertStatus(200);
    }

    /**@test */
    public function test_try_add_partner_with_bad_data_request()
    {
        $response = $this->json('POST', '/api/partner', []);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['nip', 'description', 'name']);
    }

    /**@test */
    public function test_try_add_partner_with_valid_data_request()
    {
        $response = $this->json('POST', '/api/partner', [
            'name' => 'worksmile',
            'description' => 'opis dla firmy',
            'nip' => '5272761750'
        ]);
        $response->assertStatus(201);
        $response->assertSee('uuid');
    }

    /**@test */
    public function test_try_add_and_get_partner_with_valid_data_request()
    {
        $response = $this->json('POST', '/api/partner', [
            'name' => 'worksmile',
            'description' => 'opis dla firmy',
            'nip' => '5272761750'
        ]);
        $response->assertStatus(201);
        $response->assertSee('uuid');
        $data = json_decode((string)$response->decodeResponseJson()->json);




        $response = $this->json('GET', '/api/partner/' . ($data->uuid));
        $response->assertStatus(200);
        $response->assertSee('uuid');

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->where('name', 'worksmile')
                ->where('description', 'opis dla firmy')
                ->where('nip', '5272761750')
                ->etc()

        );
        return $data->uuid;
    }

    /**@test */
    public function test_try_get_partner_list()
    {
        $nip = '5272761750';

        for ($i = 0; $i < 10; $i++) {

            $response = $this->json('POST', '/api/partner', [
                'name' => $this->faker->word(),
                'description' => $this->faker->sentence(4),
                'nip' => $nip
            ]);
            $response->assertStatus(201);
        }

        $response = $this->json('GET', '/api/partner');
        $response->assertStatus(200);
        $response->assertSee('uuid');

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(10)

        );
    }

    /**@test */
    public function test_try_update_partner()
    {
        $uuid = $this->test_try_add_and_get_partner_with_valid_data_request();

        $response = $this->json('PUT', '/api/partner/'.$uuid, [
            'name' => 'worksmile',
            'description' => 'opis dla firmy',
            'nip' => '5272761750',
            'website_url' => 'www.worksmile.pl'
        ]);
        $response->assertStatus(204);

        //check updated item
        $response = $this->json('GET', '/api/partner/' . $uuid);
        $response->assertStatus(200);
        $response->assertSee('uuid');

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->where('name', 'worksmile')
                ->where('description', 'opis dla firmy')
                ->where('nip', '5272761750')
                ->where('website_url', 'www.worksmile.pl')
                ->etc()

        );

    }
}
