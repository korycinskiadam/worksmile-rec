<?php

namespace Tests\Feature;

use App\Domain\Partner\CmdPartnerCreate;
use App\Domain\Partner\CmdPartnerUpdate;
use App\Domain\Partner\EventPartnerCreated;
use App\Domain\Partner\PartnerAggrRootId;
use App\Domain\Partner\PartnerAggrRootRepository;
use DateTime;
use DateTimeInterface;
use EventSauce\EventSourcing\DefaultHeadersDecorator;
use EventSauce\EventSourcing\Header;
use EventSauce\EventSourcing\Message;
use EventSauce\LaravelEventSauce\LaravelMessageRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class EventSourcePartnerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    private LaravelMessageRepository $repository;


    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(LaravelMessageRepository::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->repository);
    }

    protected function getEventPartnerCrated(PartnerAggrRootId $id = null): Message
    {
        $time_add = (new DateTime('now'))->format(DateTimeInterface::ISO8601);

        $event = new EventPartnerCreated(
            $this->faker->word,
            $this->faker->sentence(4),
            '5272761750',
            $this->faker->url(),
            $time_add
        );
        $id = $id ?? PartnerAggrRootId::create();

        return (new DefaultHeadersDecorator())
            ->decorate(new Message($event, [Header::AGGREGATE_ROOT_ID => $id]));
    }

    /** @test */
    public function it_can_persists_messages()
    {
        $message = $this->getEventPartnerCrated();

        $this->repository->persist($message);

        $this->assertDatabaseHas('domain_messages', [
            'id' => 1,
            'event_type' => 'app.domain.partner.event_partner_created',
        ]);
    }
    /** @test */
    public function events_stored_in_database_post()
    {
        $response = $this->json('POST', '/api/partner', [
            'name' => 'worksmile',
            'description' => 'opis dla firmy',
            'nip' => '5272761750'
        ]);
        $response->assertStatus(201);
        $response->assertSee('uuid');
        $data = json_decode((string)$response->decodeResponseJson()->json);

        $this->assertDatabaseHas('partner_aggr_root_domain_messages', [
            'event_stream' => $data->uuid,
            'event_type' => 'app.domain.partner.event_partner_created',
        ]);
    }

    /** @test */
    public function events_stored_in_database_post_and_put()
    {
        $response = $this->json('POST', '/api/partner', [
            'name' => 'worksmile_post',
            'description' => 'opis dla firmy',
            'nip' => '5272761750'
        ]);
        $response->assertStatus(201);
        $response->assertSee('uuid');
        $data = json_decode((string)$response->decodeResponseJson()->json);

        $response = $this->json('PUT', '/api/partner/' . $data->uuid, [
            'name' => 'worksmile_put',
            'description' => 'opis dla firmy',
            'nip' => '5272761750',
            'website_url' => 'www.worksmile.pl'
        ]);
        $response->assertStatus(204);

        $this->assertDatabaseHas('partner_aggr_root_domain_messages', [
            'event_stream' => $data->uuid,
            'event_type' => 'app.domain.partner.event_partner_created',
        ]);

        $this->assertDatabaseHas('partner_aggr_root_domain_messages', [
            'event_stream' => $data->uuid,
            'event_type' => 'app.domain.partner.event_partner_updated',
        ]);

        $this->assertDatabaseCount('partner_aggr_root_domain_messages',2);


    }

    /**$test */
    public function test_flow_command_created()
    {
       $time_add = (new DateTime('now'))->format(DateTimeInterface::ISO8601);

       $partnerIdentifier = PartnerAggrRootId::create(); 

        $cmd = new CmdPartnerCreate(
            $partnerIdentifier,
            $this->faker->word,
            $this->faker->sentence(4),
            '5272761750',
            $this->faker->url(),
            $time_add
        );

        $this->handleEvent($cmd);

        $this->assertDatabaseHas('partner_aggr_root_domain_messages', [
            'event_stream' => $partnerIdentifier->toString(),
            'event_type' => 'app.domain.partner.event_partner_created',
        ]);

        $this->assertDatabaseHas('partners', [
            'uuid' => $partnerIdentifier->toString()
        ]);

    }

    public function test_flow_command_created_and_updated()
    {
       $time_add = (new DateTime('now'))->format(DateTimeInterface::ISO8601);

       $partnerIdentifier = PartnerAggrRootId::create(); 

        $cmd = new CmdPartnerCreate(
            $partnerIdentifier,
            $this->faker->word,
            $this->faker->sentence(4),
            '5272761750',
            $this->faker->url(),
            $time_add
        );

        $this->handleEvent($cmd);

        $cmd = new CmdPartnerUpdate(
            $partnerIdentifier,
            0,
            $this->faker->word,
            $this->faker->sentence(4),
            '5272761750',
            $this->faker->url()
        );

        $this->handleEvent($cmd);

        $this->assertDatabaseHas('partner_aggr_root_domain_messages', [
            'event_stream' => $partnerIdentifier->toString(),
            'event_type' => 'app.domain.partner.event_partner_created',
        ]);

        $this->assertDatabaseHas('partner_aggr_root_domain_messages', [
            'event_stream' => $partnerIdentifier->toString(),
            'event_type' => 'app.domain.partner.event_partner_updated',
        ]);

        $this->assertDatabaseHas('partners', [
            'uuid' => $partnerIdentifier->toString()
        ]);

        $this->assertDatabaseCount('partner_aggr_root_domain_messages',2);

    }

    protected function handleEvent($command){

        $aggregateRootId = $command->identifier();

        $repositoryAggr = new PartnerAggrRootRepository($this->repository);

        $aggregateRoot = $repositoryAggr->retrieve($aggregateRootId);

        try {
            if ($command instanceof CmdPartnerUpdate) {
                $aggregateRoot->updatePartner($command);
            }elseif($command instanceof CmdPartnerCreate){
                $aggregateRoot->createPartner($command);
            }
        } finally {
            /*
             * Apply the new events and persist them.
             */
            $repositoryAggr->persist($aggregateRoot);
        }
    } 
}
