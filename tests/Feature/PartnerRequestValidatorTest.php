<?php

namespace Tests\Unit;

use App\Http\Requests\PartnerRequest;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;
use Tests\CreatesApplication;

class PartnerRequestValidatorTest extends TestCase
{
    use CreatesApplication;
    public function setUp(): void
    {
        parent::setUp();
       
    }


    /** @test */
    public function test_validator_bad_nip()
    {
        
       $request = new PartnerRequest();
       $rules = $request->rules();
       $validator = Validator::make([
        'name'=>'aaa',
        'description' => 'opis musi byc',
        'nip' => '8212534141'

       ],$rules);

       $this->assertFalse($validator->passes());
       $this->assertContains('nip', $validator->errors()->keys());
    }

    /** @test */
    public function test_validator_ok_nip()
    {
        
       $request = new PartnerRequest();
       $rules = $request->rules();
       $validator = Validator::make([
        'nip' => '118-16-57-766'

       ],$rules);

       
       $this->assertNotContains('nip', $validator->errors()->keys());
    }

     /** @test */
     public function test_validator_request_empty()
     {
         
        $request = new PartnerRequest();
        $rules = $request->rules();
        $validator = Validator::make([
            
 
        ],$rules);
 
        $this->assertFalse($validator->passes());
        $this->assertContains('nip', $validator->errors()->keys());
        $this->assertContains('description', $validator->errors()->keys());
        $this->assertContains('name', $validator->errors()->keys());
     }

     /** @test */
     public function test_validator_request_is_ok()
     {
         
        $request = new PartnerRequest();
        $rules = $request->rules();
        $validator = Validator::make([
            'name'=>'aaa',
            'description' => 'opis musi byc',
            'nip' => '118-16-57-766'
 
        ],$rules);
 
        $this->assertTrue($validator->passes());
        $this->assertNotContains('nip', $validator->errors()->keys());
        $this->assertNotContains('description', $validator->errors()->keys());
        $this->assertNotContains('name', $validator->errors()->keys());
     }

     /** @test */
     public function test_validator_request_empty_description()
     {
         
        $request = new PartnerRequest();
        $rules = $request->rules();
        $validator = Validator::make([
            'name'=>'aaa',
            'description' => '',
            'nip' => '118-16-57-766'
 
        ],$rules);
 
        $this->assertFalse($validator->passes());
        $this->assertNotContains('nip', $validator->errors()->keys());
        $this->assertContains('description', $validator->errors()->keys());
        $this->assertNotContains('name', $validator->errors()->keys());
     }
}
