<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NipRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $nip = str_replace("-","", $value);

                if (strlen($nip)!=10) return false;

                $weights = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
                
                
                if (strlen($nip) == 10 && is_numeric($nip)) {	 
                    $sum = 0;
                    for($i = 0; $i < 9; $i++)
                        $sum += $nip[$i] * $weights[$i];
                    return ($sum % 11) == $nip[9];
                }

                return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Sorry, NIP failed validation!';
    }
}
