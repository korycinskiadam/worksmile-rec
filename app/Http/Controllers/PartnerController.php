<?php

namespace App\Http\Controllers;

use App\Domain\Partner\CmdPartnerCreate;
use App\Domain\Partner\CmdPartnerUpdate;
use App\Domain\Partner\PartnerAggrRoot;
use App\Domain\Partner\PartnerAggrRootId;
use App\Domain\Partner\PartnerCmdHandler;
use App\Http\Requests\PartnerRequest;
use App\Models\Partner as PartnerModel;
use DateTime;
use DateTimeInterface;
use Generator;

class PartnerController extends Controller
{

    protected $commandHandler;

    public function __construct(PartnerCmdHandler $commandHandler)
    {
        $this->commandHandler = $commandHandler;
    }


    public function store(PartnerRequest $request, PartnerCmdHandler $commandHandler)
    {

        $partnerIdentifier = PartnerAggrRootId::create();      

        $time_add = (new DateTime('now'))->format(DateTimeInterface::ISO8601);

        $cmd = new CmdPartnerCreate(
            $partnerIdentifier,
            $request->name,
            $request->description,
            $request->nip,
            $request->website_url ?? "",
            $time_add
        );

        $commandHandler->handle($cmd);

        return response(['uuid' => $partnerIdentifier->toString()],201);

      
    }

    public function getList(){
        return response(PartnerModel::get(),200); 
    }

    public function getItem($identifier){
        $partnerIdentifier = PartnerAggrRootId::fromString($identifier);
        $item = PartnerModel::uuid($partnerIdentifier->toString());

        $aggregateRoot = $this->commandHandler->getRepository()->retrieve($partnerIdentifier);
        
        


        if($item)
            return response($item,200);

        return response(null,404);
    }

    public function update(PartnerRequest $request, $identifier , PartnerCmdHandler $commandHandler){

        $partnerIdentifier = PartnerAggrRootId::fromString($identifier);

        if(!(PartnerModel::uuid($partnerIdentifier->toString()))){
            return response(null,404); 
        }

        $cmd = new CmdPartnerUpdate(
            $partnerIdentifier,
            0,
            $request->name,
            $request->description,
            $request->nip,
            $request->website_url ?? ""
        );

        $commandHandler->handle($cmd);
        return response(null,204);

    }
}
