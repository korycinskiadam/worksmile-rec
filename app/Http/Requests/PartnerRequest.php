<?php

namespace App\Http\Requests;

use App\Rules\NipRule;
use Illuminate\Foundation\Http\FormRequest;

class PartnerRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nip' => ['required', new NipRule()],
            'name' => 'required',
            'description' => 'required'
        ];
    }

    public function response(array $errors) {

        // Put whatever response you want here.
        return new JsonResponse([
            'status' => '422',
            'errors' => $errors,
        ], 422);
    }
}
