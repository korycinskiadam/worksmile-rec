<?php

declare(strict_types=1);

namespace App\Domain\Partner;

use EventSauce\EventSourcing\Serialization\SerializablePayload;

final class EventPartnerCreated implements SerializablePayload
{
    private string $name;

    private string $description;

    private string $nip;

    private string $website_url;

    private string $date_add;

    public function __construct(
        string $name,
        string $description,
        string $nip,
        string $website_url,
        string $date_add
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->nip = $nip;
        $this->website_url = $website_url;
        $this->date_add = $date_add;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function nip(): string
    {
        return $this->nip;
    }

    public function website_url(): string
    {
        return $this->website_url;
    }

    public function date_add(): string
    {
        return $this->date_add;
    }

    public static function fromPayload(array $payload): SerializablePayload
    {
        return new EventPartnerCreated(
            (string) $payload['name'],
            (string) $payload['description'],
            (string) $payload['nip'],
            (string) $payload['website_url'],
            (string) $payload['date_add']
        );
    }

    public function toPayload(): array
    {
        return [
            'name' => (string) $this->name,
            'description' => (string) $this->description,
            'nip' => (string) $this->nip,
            'website_url' => (string) $this->website_url,
            'date_add' => (string) $this->date_add,
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public static function withNameAndDescriptionAndNipAndWebsite_urlAndDate_add(string $name, string $description, string $nip, string $website_url, string $date_add): EventPartnerCreated
    {
        return new EventPartnerCreated(
            $name,
            $description,
            $nip,
            $website_url,
            $date_add
        );
    }
}