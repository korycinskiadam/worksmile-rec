<?php 
declare(strict_types=1);

namespace App\Domain\Partner;

use EventSauce\EventSourcing\Serialization\SerializablePayload;

final class EventPartnerUpdated implements SerializablePayload
{
    private int $partner_id;

    private string $name;

    private string $description;

    private string $nip;

    private string $website_url;

    public function __construct(
        int $partner_id,
        string $name,
        string $description,
        string $nip,
        string $website_url
    ) {
        $this->partner_id = $partner_id;
        $this->name = $name;
        $this->description = $description;
        $this->nip = $nip;
        $this->website_url = $website_url;
    }

    public function partner_id(): int
    {
        return $this->partner_id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function nip(): string
    {
        return $this->nip;
    }

    public function website_url(): string
    {
        return $this->website_url;
    }

    public static function fromPayload(array $payload): SerializablePayload
    {
        return new EventPartnerUpdated(
            (int) $payload['partner_id'],
            (string) $payload['name'],
            (string) $payload['description'],
            (string) $payload['nip'],
            (string) $payload['website_url']
        );
    }

    public function toPayload(): array
    {
        return [
            'partner_id' => (int) $this->partner_id,
            'name' => (string) $this->name,
            'description' => (string) $this->description,
            'nip' => (string) $this->nip,
            'website_url' => (string) $this->website_url,
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public static function withPartner_idAndNameAndDescriptionAndNipAndWebsite_url(int $partner_id, string $name, string $description, string $nip, string $website_url): EventPartnerUpdated
    {
        return new EventPartnerUpdated(
            $partner_id,
            $name,
            $description,
            $nip,
            $website_url
        );
    }
}
