<?php

declare(strict_types=1);

namespace App\Domain\Partner;

use EventSauce\EventSourcing\Serialization\SerializablePayload;

final class CmdPartnerUpdate implements SerializablePayload
{
    private PartnerAggrRootId $identifier;

    private int $partner_id;

    private string $name;

    private string $description;

    private string $nip;

    private string $website_url;

    public function __construct(
        PartnerAggrRootId $identifier,
        int $partner_id,
        string $name,
        string $description,
        string $nip,
        string $website_url
    ) {
        $this->identifier = $identifier;
        $this->partner_id = $partner_id;
        $this->name = $name;
        $this->description = $description;
        $this->nip = $nip;
        $this->website_url = $website_url;
    }

    public function identifier(): PartnerAggrRootId
    {
        return $this->identifier;
    }

    public function partner_id(): int
    {
        return $this->partner_id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function nip(): string
    {
        return $this->nip;
    }

    public function website_url(): string
    {
        return $this->website_url;
    }

    public static function fromPayload(array $payload): SerializablePayload
    {
        return new CmdPartnerUpdate(
            new PartnerAggrRootId($payload['identifier']),
            (int) $payload['partner_id'],
            (string) $payload['name'],
            (string) $payload['description'],
            (string) $payload['nip'],
            (string) $payload['website_url']
        );
    }

    public function toPayload(): array
    {
        return [
            'identifier' => new PartnerAggrRootId($this->identifier),
            'partner_id' => (int) $this->partner_id,
            'name' => (string) $this->name,
            'description' => (string) $this->description,
            'nip' => (string) $this->nip,
            'website_url' => (string) $this->website_url,
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public static function withIdentifierAndPartner_idAndNameAndDescriptionAndNipAndWebsite_url(PartnerAggrRootId $identifier, int $partner_id, string $name, string $description, string $nip, string $website_url): CmdPartnerUpdate
    {
        return new CmdPartnerUpdate(
            $identifier,
            $partner_id,
            $name,
            $description,
            $nip,
            $website_url
        );
    }
}