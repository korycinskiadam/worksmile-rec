<?php

declare(strict_types=1);

namespace App\Domain\Partner;


use App\Domain\Partner\EventPartnerUpdated;
use EventSauce\EventSourcing\AggregateRoot;
use EventSauce\EventSourcing\AggregateRootBehaviour;

final class PartnerAggrRoot implements AggregateRoot
{
    use AggregateRootBehaviour;

    public function updatePartner(CmdPartnerUpdate $command){
        $event = new EventPartnerUpdated(
            $command->partner_id(), 
            $command->name(), 
            $command->description(),
            $command->nip(), 
            $command->website_url()
        );
        
        $this->recordThat($event);   
    }

    public function applyEventPartnerUpdated(EventPartnerUpdated $event){
        $this->recordedEvents;
    }

    public function createPartner(CmdPartnerCreate $command){
        $event = new EventPartnerCreated(
            $command->name(), 
            $command->description(),
            $command->nip(), 
            $command->website_url(), 
            $command->date_add()
        );
        
        $this->recordThat($event);   
    }

    public function applyEventPartnerCreated(){
        
    }
}
