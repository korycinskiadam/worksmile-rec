<?php 

declare(strict_types=1);


namespace App\Domain\Partner;



class PartnerCmdHandler{
    /** @var \EventSauce\EventSourcing\AggregateRootRepository */
    private $repository;

    public function __construct(PartnerAggrRootRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle($command){

        $aggregateRootId = $command->identifier();

        $aggregateRoot = $this->repository->retrieve($aggregateRootId);

        try {
            if ($command instanceof CmdPartnerUpdate) {
                $aggregateRoot->updatePartner($command);
            }elseif($command instanceof CmdPartnerCreate){
                $aggregateRoot->createPartner($command);
            }
        } finally {
            /*
             * Apply the new events and persist them.
             */
            $this->repository->persist($aggregateRoot);
        }
    }

    public function getRepository(){
        return $this->repository;
    }
}