<?php

declare(strict_types=1);

namespace App\Domain\Partner;

use App\Domain\Partner\Consumers\PartnerProjector;
use EventSauce\LaravelEventSauce\AggregateRootRepository;

/** @method \App\Domain\Partner\PartnerAggrRoot retrieve(\App\Domain\Partner\PartnerAggrRootId $aggregateRootId) */
final class PartnerAggrRootRepository extends AggregateRootRepository
{
    /** @var string */
    protected static string $inputFile = __DIR__ . '/__partner_commands_and_events.yml';

    /** @var string */
    protected static string $outputFile = __DIR__ . '/__commands-and-events.php';

    protected string $aggregateRoot = PartnerAggrRoot::class;

    protected string $table = 'partner_aggr_root_domain_messages';

    protected array $consumers = [PartnerProjector::class];
}
