<?php

namespace App\Domain\Partner\Consumers;

use App\Domain\Partner\EventPartnerCreated;
use App\Domain\Partner\EventPartnerUpdated;
use App\Models\Partner;
use EventSauce\EventSourcing\Consumer;
use EventSauce\EventSourcing\Message;

class PartnerProjector implements Consumer{

    public function handle(Message $message){
        $event = $message->event();

        $uuid = $message->aggregateRootId()->toString();
        
        if($event instanceof EventPartnerCreated){
            Partner::create([
                'uuid' => $uuid,
                'name'=> $event->name(),
                'description'=> $event->description(),
                'nip'=> $event->nip(),
                'website_url'=> $event->website_url(),
                'date_add'=> $event->date_add()
            ]);
            return;
        }
        
        if($event instanceof EventPartnerUpdated){
            $partner = Partner::uuid($uuid);
            $partner->name = $event->name();
            $partner->description = $event->description();
            $partner->nip = $event->nip();
            $partner->website_url = $event->website_url();
            $partner->save();
            return;
        }

        return;
    }
}