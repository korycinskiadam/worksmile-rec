<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory;

    protected $table = 'partners';

    protected $fillable = [
        'uuid', 'name', 'description', 'nip', 'website_url', 'date_add'
    ];

    protected $hidden = ['id'];

    public static function uuid(string $uuid)
    {

        return static::where('uuid', $uuid)->first();
    }
}
