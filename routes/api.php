<?php

use App\Http\Controllers\PartnerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/cb', function (Request $request) {
    return database_path('database.sqlite');
});

Route::post('/partner', [PartnerController::class, 'store']);
Route::get('/partner', [PartnerController::class, 'getList']);
Route::get('/partner/{id}', [PartnerController::class, 'getItem']);
Route::put('/partner/{id}', [PartnerController::class, 'update']);